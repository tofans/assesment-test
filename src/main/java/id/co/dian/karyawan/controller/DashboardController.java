package id.co.dian.karyawan.controller;

import java.security.Principal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DashboardController {



    @GetMapping("/")
    public String index(Principal principal,Model model) {

        return "layout";
    }

    @GetMapping("/viewIndex")
    public String getViewIndex(Principal principal,Model model) {
        return "dashboard/index";
    }

    @GetMapping("/karyawan")
    public String getAll(Principal principal,Model model) {

        return "layout";
    }

    @GetMapping("/karyawan/viewIndex")
    public String getViewIndexDashboard(Principal principal,Model model) {
        return "dashboardKaryawan/index";
    }
}
