package id.co.dian.karyawan.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.Principal;
import java.util.List;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import id.co.dian.karyawan.dto.UserDto;
import id.co.dian.karyawan.model.Group;
import id.co.dian.karyawan.model.User;
import id.co.dian.karyawan.repository.UserRepository;
import id.co.dian.karyawan.repository.UserRepository.getAl_Users;
import id.co.dian.karyawan.service.GroupService;
import id.co.dian.karyawan.service.UserService;
import id.co.dian.karyawan.service.UserServiceImpl;

@Controller
@RequestMapping("/user")
public class UserController {

	private static final String UPLOAD_DIR = "src\\main\\resources\\static\\upload\\";

	@Autowired
	private UserService userService;
	@Autowired
	private GroupService groupService;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserServiceImpl userImpl;

	@GetMapping("/")
	public String getAll(Principal principal, Model model) {
		return "layout";

	}

	@GetMapping("/viewIndex")
	public String getAllAsComponent(Principal principal, Model model) {
		List<Group> group = groupService.findAll();
		return "preferensi/user/index";

	}

	@GetMapping("/add")
	public String regist(Principal principal, Model model) {
    	return "layout";
	}

	@GetMapping("/add/viewIndex")
	public String getAddContent(Model model) {
		List<Group> group = groupService.findAll();
		return "preferensi/user/add";
	}

	@GetMapping(value = "/edit/{id}")
	public String edit(Principal principal, Model model, @PathVariable("id") String id) {
		return "layout";
	}

	@GetMapping(value = "/edit/{id}/viewIndex")
	public String getEditContent(@PathVariable("id") String id, Model model) {
		List<getAl_Users> users = userRepository.getForEdit(Integer.parseInt(id));
		String username = "";
		String idGroup = "";
		String nameGroup = "";
		String telp = "";
		String email = "";
		if(!users.isEmpty()) {
			for(getAl_Users val : users) {
				username = val.getUsername();
				idGroup = val.getIdGroup();
				nameGroup = val.getNameGroup();
				telp = val.getTelp();
				email = val.getEmail();
			}
		}
		return "preferensi/user/edit";
	}

	@PostMapping("/save")
	public String create(Principal principal, HttpServletRequest request, RedirectAttributes redirectAttributes) {
		String username = principal.getName();
		User user = userService.findByUsername(username);
		UserDto dto = new UserDto();
		Integer id = request.getParameter("id").equals("") ? null : Integer.parseInt(request.getParameter("id"));
		dto.setIdUsers(id);
		dto.setUsername(request.getParameter("username").trim());
		dto.setPassword(request.getParameter("new_password").trim());
		dto.setEmail(request.getParameter("email").trim());
		dto.setTelp(request.getParameter("telp").trim());
		dto.setUserCreated(user.getIdUsers());
		dto.setIdGroup(Integer.parseInt(request.getParameter("group").trim()));

		String newPass = request.getParameter("new_password");
		String confirmPass = request.getParameter("confirm_password");
		if (newPass.equals(confirmPass)) {
			redirectAttributes.addFlashAttribute("message", "Sukses Simpan Data Pengguna.");
			redirectAttributes.addFlashAttribute("alertClass", "alert-success");
			return "redirect:/user/viewIndex";
		} else {
			redirectAttributes.addFlashAttribute("message", "Gagal Simpan! password salah.");
			redirectAttributes.addFlashAttribute("alertClass", "alert-danger");
			if(id == null) {
				return "redirect:/user/add/viewIndex";
			}
			return "redirect:/user/edit/"+id.toString()+"/viewIndex";
		}
	}

	@GetMapping("/delete/{id}/viewIndex")
	public String delete(@PathVariable("id") String id, RedirectAttributes redirectAttributes) {
		try {
			Integer delete = userImpl.delete(Integer.parseInt(id));
			if(delete == 1) {
				redirectAttributes.addFlashAttribute("message", "Sukses Menghapus Pengguna.");
				redirectAttributes.addFlashAttribute("alertClass", "alert-success");
				return "redirect:/user/viewIndex";
			}
			redirectAttributes.addFlashAttribute("message", "Gagal Menghapus Data Pengguna.");
			redirectAttributes.addFlashAttribute("alertClass", "alert-success");
			return "redirect:/user/viewIndex";
		}catch(Exception e){
			redirectAttributes.addFlashAttribute("message", "error exception.");
			redirectAttributes.addFlashAttribute("alertClass", "alert-success");
			return "redirect:/user/viewIndex";
		}
	}

	@GetMapping("/checkUserName")
	@ResponseBody
	public boolean  checkUser(@RequestParam("userName") String userName, RedirectAttributes redirectAttributes){

		User user = null;
		if (user != null) {

			 return true ;// method return bolean if user exist or non in database.
		}else {
			return false;
		}
	}

	@GetMapping("/profile/")
	public String getProfile(Principal principal, Model model) {
		return "layout";
	}

	@GetMapping("/profile/viewIndex")
	public String getUserProfile(Principal principal, Model model) {
//		List<Group> group = groupService.findAll();
		User user = userService.findByUsername(principal.getName());
		model.addAttribute("userData", user);
		String filePhoto = "";
		if (user.getFilePhoto().equals("")) {
			filePhoto = "~/upload/noimage.jpg";
		} else {
			filePhoto = "~/upload/" + user.getFilePhoto();
		}
		model.addAttribute("pathPhoto", filePhoto);
		return "profile/index";

	}

	@PostMapping("/upload")
	public String uploadFile(@RequestParam("file") MultipartFile file, RedirectAttributes attributes, Principal principal) {
		if (file.isEmpty()) {
			attributes.addFlashAttribute("message", "Please select a file to upload.");
			return "redirect:/user/profile/";
		}
		String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
		try {
			String fileData = pathLoc() + UPLOAD_DIR + fileName;
			Path path = Paths.get(fileData);
			Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
		}
		transformSaveObject(fileName, principal.getName());
		attributes.addFlashAttribute("message", "You successfully uploaded " + fileName + '!');
		return "redirect:/user/profile/";
	}

	@GetMapping("/remove")
	public String deletePhoto(Principal principal, RedirectAttributes attributes) {
		transformSaveObject("", principal.getName());
		attributes.addFlashAttribute("message", "You successfully remove photo !");
		return "redirect:/user/profile/";
	}

	private void transformSaveObject(String photo, String principal) {
		UserDto userDto = new UserDto();
		User user = userService.findByUsername(principal);
		userDto.setIdUsers(user.getIdUsers());
		userDto.setFilePhoto(photo);
		userDto.setEmail(user.getEmail());
		userDto.setIdGroup(user.getIdGroup());
		userDto.setUsername(user.getUsername());
		userDto.setPassword(user.getPassword());
		userDto.setTelp(user.getTelp());
		userDto.setUserCreated(user.getUserCreated());
		userService.save(userDto);
	}

	private String pathLoc() throws URISyntaxException {
		URL url = getClass().getResource("/");
		File file = new File(url.toURI()).getAbsoluteFile();
		return file.getAbsolutePath().split("target")[0];
	}
}
