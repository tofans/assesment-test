package id.co.dian.karyawan.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import id.co.dian.karyawan.repository.GroupRepository;
import id.co.dian.karyawan.service.GroupServiceImpl;


@RestController
@RequestMapping("/role")
public class GroupRestController {
	
	@Autowired
	private GroupRepository groupRepo;
	
	@Autowired
	private GroupServiceImpl groupImp;
	
	@PostMapping("/getJson")
	public String getJson(HttpServletRequest servletRequest,@RequestBody String json) {
		try {
			Gson gson = new Gson();
			String draw = servletRequest.getParameter("draw");
			String search = servletRequest.getParameter("search[value]");
			String start = servletRequest.getParameter("start");
			String length = servletRequest.getParameter("length");

			List<Object> results = groupImp.getGroups(search, Integer.parseInt(start), Integer.parseInt(length));
			Integer recordsFiltered = groupImp.getCountFilter(search);
			return "{\"draw\": "+Integer.parseInt(draw)+", \"recordsTotal\": "+groupRepo.getTotal()+", \"recordsFiltered\": "+recordsFiltered+", \"data\": "+gson.toJson(results)+"}";
		}catch(HibernateException e){
			return "{\"code\":\"96\",\"info\":\"Error\"}";
		}catch(Exception e){
			return "{\"code\":\"99\",\"info\":\"Invalid parameter\"}";
		}
	}
}
