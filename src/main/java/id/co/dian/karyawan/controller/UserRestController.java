package id.co.dian.karyawan.controller;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import id.co.dian.karyawan.repository.UserRepository;
import id.co.dian.karyawan.service.UserServiceImpl;



@RestController
@RequestMapping("/user")
public class UserRestController {

	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private UserServiceImpl userImpl;

	@PostMapping("/getJson")
	public String getJson(HttpServletRequest servletRequest,@RequestBody String json) {
		try {
			Gson gson = new Gson();
			String draw = servletRequest.getParameter("draw");
			String search = servletRequest.getParameter("search[value]");
			String start = servletRequest.getParameter("start");
			String length = servletRequest.getParameter("length");
			String filterUsername = servletRequest.getParameter("filterUsername");
			String filterGroup = servletRequest.getParameter("filterGroup");

			List<Object> results = userImpl.getUsers(filterUsername, filterGroup, Integer.parseInt(start), Integer.parseInt(length));
			Integer recordsFiltered = userImpl.getCountFilterUsers(filterUsername, filterGroup);
			return "{\"draw\": "+Integer.parseInt(draw)+", \"recordsTotal\": "+userRepo.getTotal()+", \"recordsFiltered\": "+recordsFiltered+", \"data\": "+gson.toJson(results)+"}";
		}catch(HibernateException e){
			return "{\"code\":\"96\",\"info\":\"Error\"}";
		}catch(Exception e){
			return "{\"code\":\"99\",\"info\":\"Invalid parameter\"}";
		}
	}
	
	@PostMapping("/comboGroup")
	public String comboGroup(HttpServletRequest servletRequest) {
		try {
			Gson gson = new Gson();
			String search = servletRequest.getParameter("search") == null ? "" : servletRequest.getParameter("search");
			
			List<Object> results = userImpl.getComboGroup(search);
			return "{\"results\": "+gson.toJson(results)+"}";
		}catch(HibernateException e){
			return "{\"code\":\"96\",\"info\":\"Error\"}";
		}catch(Exception e){
			return "{\"code\":\"99\",\"info\":\"Invalid parameter\"}";
		}
	}
}
