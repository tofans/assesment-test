package id.co.dian.karyawan.model;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import lombok.Data;

@Data
@Entity
@Table(name = "t2_employee")
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @NotEmpty
  @Column(length = 100)
  private String name;

  private Date birthDate;

  private int positionId;

  @Column(unique = true, length = 18)
  private int idNumber;

  private int gender;

  private int isDelete = 0;

  public Employee() {

  }
}
