package id.co.dian.karyawan.model;


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;


import lombok.Data;

@Data
@Entity
@Table(name = "al_users")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idUsers;

	@NotEmpty
	private String username;
	@NotEmpty
	private String password;
	private int idGroup;
	private String telp;
	private String email;
	@Column(columnDefinition = "TINYINT")
	private boolean loginStatus;
	private String uniqueCode;
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
	private int userCreated;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updated;
	private int userUpdated;
	private String filePhoto;

	public User() {

	}

}
