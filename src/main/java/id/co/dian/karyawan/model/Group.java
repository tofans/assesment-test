package id.co.dian.karyawan.model;



import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Data
@Entity
@Table(name = "al_group")
public class Group {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idGroup;
	private String nameGroup;
	private String descGroup;
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
	private int userCreated;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updated;
	private int userUpdated;

	public Group() {

	}

}
