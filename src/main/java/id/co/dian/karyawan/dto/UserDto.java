package id.co.dian.karyawan.dto;

import lombok.Data;

@Data
public class UserDto {
  private Integer idUsers;
  private String username;
  private String password;
  private String email;
  private String telp;
  private int userCreated;
  private Integer idGroup;
  private String filePhoto;
}
